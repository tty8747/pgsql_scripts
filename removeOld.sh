#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

source ./.env

if [ -f "$FILELOCK" ]; then
    printf "%s exists! Remove it and try again" "$FILELOCK"
    exit 191
else
    touch "$FILELOCK"
fi

chmod -R 2770 "$BACKUP_ROOT"/daily/

# It returns nonzero code:
# find "$BACKUP_ROOT/daily" -mtime +20 -exec rm -rf {} \;
# find "$BACKUP_ROOT/weekly" -mtime +180 -exec rm -rf {} \;

find "$BACKUP_ROOT/daily" -mtime +20 -delete
find "$BACKUP_ROOT/weekly" -mtime +180 -delete

rm -f "$FILELOCK"
exit 0
