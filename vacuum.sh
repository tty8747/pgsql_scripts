#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

source ./.env

if [ -f "$FILELOCK" ]; then
    printf "%s exists! Remove it and try again" "$FILELOCK"
    exit 191
else
    touch "$FILELOCK"
fi

DB_LIST=$(/usr/bin/psql -l -t -U "$PGUSER" |  /usr/bin/cut -d'|' -f1 | sed '/^ *$/d' | grep -Eiv "$EXCLUDE")
for DB in $DB_LIST
  do

    logfile=$DB.log.$$.$RANDOM
    time (/usr/bin/psql -t -U "$PGUSER" --dbname "$DB" --command "VACUUM(FULL, VERBOSE, ANALYZE);") > /tmp/"$logfile" 2>&1

  done

rm -f "$FILELOCK"
exit 0
