#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace

source ./.env

if [ -f "$FILELOCK" ]; then
    printf "%s exists! Remove it and try again\n" "$FILELOCK"
    exit 191
else
    touch "$FILELOCK"
fi

if [ "$(date +%w)" -eq 5 ]; then 
    BACKUP_DIR="$BACKUP_ROOT/weekly/$(date +%F)"
else
    BACKUP_DIR="$BACKUP_ROOT/daily/$(date +%F)"
fi

mkdir -p "$BACKUP_DIR"

DB_LIST=$(/usr/bin/psql -l -t -U "$PGUSER" |  /usr/bin/cut -d'|' -f1 | sed '/^ *$/d' | grep -Eiv "$EXCLUDE")

printf "List of databases that will be use:\n"
for DB in $DB_LIST; do 
    printf "|%s" "$DB"; done

printf "\n"
rm -f "$FILELOCK"
exit 0
