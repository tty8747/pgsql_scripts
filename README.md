# How to start:

- Clone this repository.
- Put your values into `.env` file:
  - login
  - password
  - backup folder
  - etc
- Run the `testConn.sh` script and put the outputs into exclude variable. After that, you should remove work databases from exclude variable.
- Add jobs into your `crontab`. For example:
```bash
30 03 * * * bash -c "cd /root/pgsql_scripts/ && /root/pgsql_scripts/backup.sh"
30 07 * * * bash -c "cd /root/pgsql_scripts/ && /root/pgsql_scripts/reindex.sh"
00 14 * * * bash -c "cd /root/pgsql_scripts/ && /root/pgsql_scripts/removeOld.sh"
00 19 * * 6 bash -c "cd /root/pgsql_scripts/ && /root/pgsql_scripts/vacuum.sh"
```
- Enjoy!

### PS:
- Logfiles will be put into `/tmp` and it isn't deleted when the script will be finished.  
- There aren't markers for log files.
